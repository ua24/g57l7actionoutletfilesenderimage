//
//  ViewController.swift
//  G57L7
//
//  Created by Ivan Vasilevich on 10/17/17.
//  Copyright © 2017 Smoosh Labs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	@IBOutlet weak var bottomImageView: UIImageView!
	@IBOutlet weak var headerLabel: UILabel!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		var runCount = UserDefaults.standard.integer(forKey: "runsCount")
		print("run# \(runCount)")
		runCount += 1
		UserDefaults.standard.set(runCount, forKey: "runsCount")
		UserDefaults.standard.synchronize()
		view.backgroundColor = .yellow
	}

	@IBAction func buttonPressed() {
		print("Valiyanki")
		headerLabel.text = "Hello MAMA"
		headerLabel.textColor = UIColor.red
	}
	
	@IBAction func writeFile() {
		let arr = [1, 4, 9, 89] as NSArray
		arr.write(toFile: "/Users/ivanvasilevich/Desktop/myFile.plist", atomically: true)
	}
	
	@IBAction func readFile() {
		if let array = NSArray.init(contentsOfFile: "/Users/ivanvasilevich/Desktop/myFile.plist") as? Array<Int> {
			print(array)
			let abcd = array
		}
		
	}
	
	@IBAction func changePicture(_ sender: UIButton) {
		var imageName = ""
		let fara = sender.tag == 1//sender.currentTitle?.contains("Light") ?? false
//		if  fara {
//			imageName = "fara"
//		}
//		else {
//			imageName = "botik"
//		}
//		fara ? imageName = "fara" : imageName = "botik"
		imageName = fara  ? "fara" : "botik"
		let image = UIImage.init(named: imageName)// "botik"
		bottomImageView.image = image
		
	}
}

